<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form>
		<label>First Name:</label><br><br>
			<input type="text" name="fname"><br><br>
		<label>Last Name:</label><br><br>
			<input type="text" name="lname"><br><br>
	</form>

	<form>
		<label>Gender:</label><br><br>
		<input type="radio" name="Gender">
		<label>Male</label><br>
		<input type="radio" name="Gender">
		<label>Female</label><br>
		<input type="radio" name="Gender">
		<label>Other</label><br><br>	
	</form>

	<form>
		<label>Nationality:</label><br><br>
		<select>
			<option>Indonesian</option>
			<option>Others</option>
		</select><br><br>
	</form>

	<form>
		<label>Language Spoken:</label><br><br>
		<input type="radio" name="Language">
		<label>Bahasa Indonesia</label><br>
		<input type="radio" name="Language">
		<label>English</label><br>
		<input type="radio" name="Language">
		<label>Other</label><br><br>	
	</form>

	<form action="/welcome">
		<label>Bio:</label><br><br>
		<textarea rows="10" cols="30"></textarea><br>
		<input type="submit" name="Submit">
	</form>

</body>
</html>